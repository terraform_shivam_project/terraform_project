provider "aws" {
  region = "ap-south-1"
}                            #this is provider block

resource "aws_vpc" "my_vpc" {
  cidr_block       = var.vpc_cidr #taking refrence from variable
  enable_dns_hostnames = true #here we want to show dns name is active

  tags = {
    Name = "DEMO VPC - WEZVATECH"  #tag is nothing but name in the aws cloud
  }
}
data "aws_availability_zones" "all" {} #it will capture all the gather data from the code


#-------------------------------------------------
# Create a Public subnet on the First available AZ
#-------------------------------------------------

resource "aws_subnet" "public_ap_south_1a" {
  vpc_id     = aws_vpc.my_vpc.id                                 
  cidr_block = var.subnet1_cidr
  availability_zone = data.aws_availability_zones.all.names[0] #here taking the gather data and choosing the first availaibility zone

  tags = {
    Name = "Public Subnet - WEZVATECH"
  }
}


#-------------------------------
# Create an IGW for your new VPC
#-------------------------------
resource "aws_internet_gateway" "my_vpc_igw" {
  vpc_id = aws_vpc.my_vpc.id                            

  tags = {
    Name = "DEMO IGW - WEZVATECH"
  }
}   

#----------------------------------
# Create an RouteTable for your VPC
#----------------------------------
resource "aws_route_table" "my_vpc_public" {
    vpc_id = aws_vpc.my_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.my_vpc_igw.id
    }

    tags = {
        Name = "DEMO Public RouteTable - WEZVATECH"
    }
}

#--------------------------------------------------------------
# Associate the RouteTable to the Subnet created at ap-south-1a
#--------------------------------------------------------------
resource "aws_route_table_association" "my_vpc_ap_south_1a_public" {  #the route table association
    subnet_id = aws_subnet.public_ap_south_1a.id
    route_table_id = aws_route_table.my_vpc_public.id
}

#-----------------------------------------------------------
# CREATE THE SECURITY GROUP THAT'S APPLIED TO Web Server EC2 
#-----------------------------------------------------------
resource "aws_security_group" "instance" {   
  name = "adam-example-instance"
  vpc_id = aws_vpc.my_vpc.id

  # Allow all outbound 
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Inbound for SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  # Inbound for Web server
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#------------------
# Create EC2 Server
#------------------
resource "aws_instance" "server" {
   ami           = var.amiid
   instance_type = var.type
   key_name      = var.pemfile
   vpc_security_group_ids = [aws_security_group.instance.id]
   subnet_id = aws_subnet.public_ap_south_1a.id
   availability_zone = data.aws_availability_zones.all.names[0]
   
   associate_public_ip_address = true

   user_data = <<-EOF
               #!/bin/bash
               echo '<html><body><img src ="C:\Users\kumar shivam\Desktop\wezva.png"><marquee><h1 style="font-size:50px;color:cyan;"><u>WELCOME TO MY FIRST PROJECT</u></marquee><br><h2> PROJECT NAME - VPC with private and public subnet for web<p>Hello everyone, ths is my first project in terraform infrastructure as a code. As we know Terraform is declarative not procedural, so we dont need to worry about the steps.<h3>These are the following steps i have done to achieve this project</h3>.<ol> <li> configure our own connecton</li> <li> create VPC </li><li>Get the list of availability zone in the current region</li><li>Create public subnet on the first available AZ</li><li>Create an IGW for new VPC</li><li>Create route table</li><li>association the route table to thesubnet created</li><li>Create the security groups thats applied to web server EC2</li><li>Create EC2 server</li><li>Create a private subnet on the second AZ</li><li>Create route table for your DB</li><li>Associate the DB route table to subnet created at the ap-south-1b</li><li>Create the security group thats applied to db server EC2</li><li>Create EC2 DB server</li><br><font style="color:red;"></h3><i> DONE BY SHIVAM SHARMA</i></h3> <br><h3><b>UNDER GUIDANCE - ADAM.M(DEVOPS ARCHITECT)</b></h2><br> (WEZVA TECHNOLOGY) <font style="color:green;"> +91-9739110917 </h1> </body></html>' > index.html
               nohup busybox httpd -f -p 8080 &
              EOF

    tags = {
        Name = "Web Server - WEZVATECH"
    }
  
}

#---------------------------------------------------
# Create a Private subnet on the Second available AZ
#---------------------------------------------------
resource "aws_subnet" "private_ap_south_1b" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = var.subnet2_cidr
  availability_zone = data.aws_availability_zones.all.names[1]

  tags = {
    Name = "Private Subnet - WEZVATECH"
  }
}

#---------------------------------
# Create an RouteTable for your DB
#---------------------------------
resource "aws_route_table" "my_vpc_private" {
    vpc_id = aws_vpc.my_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        instance_id = aws_instance.server.id
    }

    tags = {
        Name = "DEMO Private RouteTable - WEZVATECH"
    }
}

#----------------------------------------------------------------
# Associate the DB RouteTable to the Subnet created at ap-south-1b
#----------------------------------------------------------------
resource "aws_route_table_association" "my_vpc_ap_south_1b_private" {
    subnet_id = aws_subnet.private_ap_south_1b.id
    route_table_id = aws_route_table.my_vpc_private.id
}

#----------------------------------------------------------
# CREATE THE SECURITY GROUP THAT'S APPLIED TO DB Server EC2
#----------------------------------------------------------
resource "aws_security_group" "db" {
  name = "adam-example-db"
  vpc_id = aws_vpc.my_vpc.id

  # Allow all outbound 
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Inbound for SSH
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    security_groups = [aws_security_group.instance.id]
  }
  
}

#---------------------
# Create EC2 DB Server
#---------------------
resource "aws_instance" "db" {
   ami           = var.amiid
   instance_type = var.type
   key_name      = var.pemfile
   vpc_security_group_ids = [aws_security_group.db.id]
   subnet_id = aws_subnet.private_ap_south_1b.id
   availability_zone = data.aws_availability_zones.all.names[1]
   
   associate_public_ip_address = true

   tags = {
       Name = "DB Server - WEZVATECH"
   }
  
}
